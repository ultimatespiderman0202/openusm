#include <gtest/gtest.h>

#include <resource_pack_streamer.h>

TEST(ResourcePartition, Construct)
{
#if 0
    resource_pack_streamer streamer {};
    EXPECT_TRUE(streamer.is_active());
    EXPECT_TRUE(streamer.is_disk_idle());
    EXPECT_TRUE(streamer.is_idle());
    EXPECT_TRUE(streamer.is_idle());
    EXPECT_EQ(streamer.get_pack_slots(), nullptr);
#endif
}

