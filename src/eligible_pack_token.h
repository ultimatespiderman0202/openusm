#pragma once

struct eligible_pack_token {
    int field_0;
    int field_4;

    eligible_pack_token() = default;

    eligible_pack_token(int a1, int a2) : field_0(a1), field_4(a2) {}
};
