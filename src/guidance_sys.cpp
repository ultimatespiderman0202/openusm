#include "guidance_sys.h"

#include "common.h"

VALIDATE_SIZE(guidance_system, 0x20);
VALIDATE_SIZE(rocket_guidance_sys, 0xA8u);
